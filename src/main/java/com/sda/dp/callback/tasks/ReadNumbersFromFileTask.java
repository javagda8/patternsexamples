package com.sda.dp.callback.tasks;

import com.sda.dp.callback.callbacks.ICallback;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadNumbersFromFileTask implements Runnable{
    private ICallback callback;

    public ReadNumbersFromFileTask(ICallback callback) {
        this.callback = callback;
    }

    @Override
    public void run() {
        List<Integer> numbers = new ArrayList<>();

        try(Scanner scanner = new Scanner(new File("out.txt"))){
            while (scanner.hasNextInt()){
                numbers.add(scanner.nextInt());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if(callback != null){
            callback.taskFinished(numbers);
        }
    }
}
