package com.sda.dp.callback.tasks;

import com.sda.dp.callback.callbacks.ICallback;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class GenerateNumbersToFileTask implements Runnable {
    private int numbersCount;
    private ICallback callback;

    public GenerateNumbersToFileTask(int numbersCount) {
        this.numbersCount = numbersCount;
    }

    public GenerateNumbersToFileTask(int numbersCount, ICallback callback) {
        this.numbersCount = numbersCount;
        this.callback = callback;
    }

    @Override
    public void run() {
        Random r = new Random();
        List<Integer> numbers = new LinkedList<>();
        for (int i = 0; i < numbersCount; i++) {
            numbers.add(r.nextInt());
        }

        try (PrintWriter writer = new PrintWriter("out.txt")) {
            while (!numbers.isEmpty()) {
                writer.print(numbers.remove(0));
                writer.print(" ");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if(callback != null){
            callback.taskFinished();
        }
    }
}
