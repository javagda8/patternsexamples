package com.sda.dp.callback.callbacks;

import java.util.List;

public class SumCallback implements ICallback {
    @Override
    public void taskFinished() {

    }

    @Override
    public void taskFinished(List<Integer> result) {
        System.out.println("Task finished");
        System.out.println("Sum:" + result.stream().mapToInt(Integer::intValue).sum());
    }
}
