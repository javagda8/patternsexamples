package com.sda.dp.callback.callbacks;

import java.util.List;

public class MeanCallback implements ICallback{
    @Override
    public void taskFinished() {

    }

    @Override
    public void taskFinished(List<Integer> result) {
        System.out.println("Task finished");
        int sum = result.stream().mapToInt(Integer::intValue).sum();
        System.out.println("Mean:" + (sum/result.size()));
    }
}
