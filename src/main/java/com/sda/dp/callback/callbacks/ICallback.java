package com.sda.dp.callback.callbacks;

import java.util.List;

public interface ICallback {
    void taskFinished();
    void taskFinished(List<Integer> result);
}
