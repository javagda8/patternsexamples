package com.sda.dp.callback.callbacks;

import java.util.List;

public class FireworksCallback implements ICallback{
    @Override
    public void taskFinished() {
        System.out.println("Fireworks!!!!!");
    }

    @Override
    public void taskFinished(List<Integer> result) {
        System.out.println("Fireworks!!!!!");
        System.out.println("Generated: " + result);
    }
}
