package com.sda.dp.callback;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        FileOperator operator = new FileOperator();

        Scanner scanner = new Scanner(System.in);

        boolean isWorking = true;
        while (isWorking){
            String line = scanner.nextLine();
            if(line.startsWith("g")){
                operator.generateNumbersIntoFile(Integer.parseInt(line.split(" ")[1]));
            }else if(line.startsWith("sum")){
                operator.readNumbersGiveSum();
            }else if(line.equals("mean")){
                operator.readNumbersGiveMean();
            }
        }
    }
}
