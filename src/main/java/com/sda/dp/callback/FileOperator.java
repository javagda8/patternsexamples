package com.sda.dp.callback;

import com.sda.dp.callback.callbacks.FireworksCallback;
import com.sda.dp.callback.callbacks.ICallback;
import com.sda.dp.callback.callbacks.MeanCallback;
import com.sda.dp.callback.callbacks.SumCallback;
import com.sda.dp.callback.tasks.GenerateNumbersToFileTask;
import com.sda.dp.callback.tasks.ReadNumbersFromFileTask;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileOperator implements ICallback {

    private ExecutorService threads = Executors.newFixedThreadPool(3);

    private int numberOfFinishedTasks = 0;
    private int numberOfStartedTasks = 0;

    public void generateNumbersIntoFile(int numbersCount/*, String fileName*/) {
        threads.submit(new GenerateNumbersToFileTask(numbersCount, this));
        numberOfStartedTasks++;
        System.out.println("Rozpoczętych zadan : " + numberOfStartedTasks);
    }

    public void readNumbersGiveSum() {
        threads.submit(new ReadNumbersFromFileTask(null));
        numberOfStartedTasks++;
        System.out.println("Rozpoczętych zadan : " + numberOfStartedTasks);
    }


    public void readNumbersGiveMean() {
        threads.submit(new ReadNumbersFromFileTask(new MeanCallback()));
        numberOfStartedTasks++;
        System.out.println("Rozpoczętych zadan : " + numberOfStartedTasks);
    }

    @Override
    public void taskFinished() {
        numberOfFinishedTasks++;
        System.out.println("Zakonczone zadanie: " + numberOfFinishedTasks);
    }

    @Override
    public void taskFinished(List<Integer> result) {

    }
}
