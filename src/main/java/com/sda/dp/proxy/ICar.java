package com.sda.dp.proxy;

public interface ICar {
    public void startCar();
    public void stopCar();
}