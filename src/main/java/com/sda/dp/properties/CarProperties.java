package com.sda.dp.properties;

public enum CarProperties {
    LIGHTS_ON,
    ENGINE_CAPACITY,
    DOORS_CLOSED,
    WINDOWS_CLOSED,
    HORSEPOWER,
    DOORSNUMBER,
    SEATS_NUMBER,
}
