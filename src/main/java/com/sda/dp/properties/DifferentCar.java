package com.sda.dp.properties;

import java.util.HashMap;
import java.util.Map;

public class DifferentCar implements IDifferentCar{
    private Map<CarProperties, Object> properties = new HashMap<>();

    public DifferentCar() {
        for (CarProperties property : CarProperties.values()) {
            properties.put(property, 0);
        }
    }

    public DifferentCar(Builder builder) {
        this.properties = new HashMap<>(builder.properties);
    }

    public <T> T getProperty(CarProperties property) {
        T value = (T) properties.get(property);
        return value;
    }

    public void setProperty(CarProperties coUstawic, Object naJakaWartosc) {
        properties.put(coUstawic, naJakaWartosc);
    }


    public Double getEngineCapacity() {
        try {
            double engineCapacity = getProperty(CarProperties.ENGINE_CAPACITY);
            return engineCapacity;
        }catch (ClassCastException cce){
            System.err.println("Error");
            return null;
        }
    }

    public void setEngineCapacity(double engineCapacity) {
        setProperty(CarProperties.ENGINE_CAPACITY, engineCapacity);
    }

    public static class Builder{
        private Map<CarProperties, Object> properties = new HashMap<>();
        public Builder() {
        }

        public Builder setProperty(CarProperties property, Object value){
            properties.put(property, value);
            return this;
        }

        public DifferentCar create(){
            return new DifferentCar(this);
        }
    }
}
