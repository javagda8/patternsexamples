package com.sda.dp.properties;

public class Main {
    public static void main(String[] args) {
        IDifferentCar car = new DifferentCar();
//        car.setProperty(CarProperties.ENGINE_CAPACITY, 2.1234);

        Double capacity = car.getEngineCapacity();

        System.out.println(capacity);

        DifferentCar.Builder builder = new DifferentCar.Builder();

        DifferentCar carX = builder.setProperty(CarProperties.ENGINE_CAPACITY, 2.0)
                .setProperty(CarProperties.SEATS_NUMBER, 2)
                .setProperty(CarProperties.LIGHTS_ON, true).create();

//        DifferentCarBefore dcb = new DifferentCarBefore()

    }
}
