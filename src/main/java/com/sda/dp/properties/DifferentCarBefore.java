package com.sda.dp.properties;

public class DifferentCarBefore {
    private boolean lightsOn;
    private double engineCapacity;
    private boolean doorsClosed;
    private boolean windowsClosed;
    private int horsepower;
    private int doorsNumber;
    private int power;


    public DifferentCarBefore(boolean lightsOn, double engineCapacity, boolean doorsClosed, boolean windowsClosed, int horsepower, int doorsNumber) {
        this.lightsOn = lightsOn;
        this.engineCapacity = engineCapacity;
        this.doorsClosed = doorsClosed;
        this.windowsClosed = windowsClosed;
        this.horsepower = horsepower;
        this.doorsNumber = doorsNumber;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public boolean isLightsOn() {
        return lightsOn;
    }

    public void setLightsOn(boolean lightsOn) {
        this.lightsOn = lightsOn;
    }

    public double getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public boolean isDoorsClosed() {
        return doorsClosed;
    }

    public void setDoorsClosed(boolean doorsClosed) {
        this.doorsClosed = doorsClosed;
    }

    public boolean isWindowsClosed() {
        return windowsClosed;
    }

    public void setWindowsClosed(boolean windowsClosed) {
        this.windowsClosed = windowsClosed;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public int getDoorsNumber() {
        return doorsNumber;
    }

    public void setDoorsNumber(int doorsNumber) {
        this.doorsNumber = doorsNumber;
    }
}

