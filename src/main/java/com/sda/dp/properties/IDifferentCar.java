package com.sda.dp.properties;

public interface  IDifferentCar {
    public void setEngineCapacity(double engineCapacity);
    public Double getEngineCapacity();
}
